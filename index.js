'use strict';

const express = require('express');
//const favicon = require('serve-favicon');
const path = require('path');
const { createServer } = require('http');
const WebSocket = require('ws');
const shell = require('shelljs');

const app = express();
app.use(express.static(path.join(__dirname, '/public')));
//app.use(favicon(path.join(__dirname,'public','favicon.ico')));

const server = createServer(app);

const wss = new WebSocket.Server({ server });

wss.on('connection', function (ws, request) {
  console.log('New connection: '+request.connection.remoteAddress);
  ws.on('close', function () {
    //console.log('Goodbye!');
  });
  ws.on('message', function incoming(message) {
    if (message == 'shutdown') {
      console.log('Shutdown!');
      shell.exec('sudo shutdown now');
    }
    if (message == 'reboot') {
      console.log('Reboot!');
      shell.exec('sudo shutdown -r now');
    }
  });
});

server.listen(8080, function () {
  console.log('Listening on https://localhost:8080');
});
